package sqlx

import "database/sql"

// PostgresDialect provides an interface into Postgres SQL servers.
type PostgresDialect struct{}

var _ Dialect = &PostgresDialect{}

// NewPostgresDialect creates a new PostgresDialect.
func NewPostgresDialect() *PostgresDialect {
	return &PostgresDialect{}
}

// EnsureMigrationMetadataTableExists will create a table for tracking migration
// metadata if one does not exist already.
func (*PostgresDialect) EnsureMigrationMetadataTableExists(tx Transaction) error {
	sqlCreate := `
		CREATE TABLE IF NOT EXISTS migration_metadata (
			partition_key VARCHAR(255) NOT NULL           PRIMARY KEY,
			timestamp     BIGINT       NOT NULL DEFAULT 0
		);
	`

	_, err := tx.Exec(sqlCreate)
	return err
}

// EnsureMigrationPartitionKeyExists will create an entry in the migration
// metadata table if one does not exist currently.
func (*PostgresDialect) EnsureMigrationPartitionKeyExists(tx Transaction, partitionKey string) error {
	sqlQuery := `
		SELECT partition_key FROM migration_metadata
		WHERE partition_key = $1;
	`

	sqlInsert := `
		INSERT INTO migration_metadata (partition_key)
		VALUES($1);
	`

	row := tx.QueryRow(sqlQuery, partitionKey)
	err := row.Scan(&partitionKey)
	if err == sql.ErrNoRows {
		_, err := tx.Exec(sqlInsert, partitionKey)
		return err
	}
	if err != nil {
		return err
	}

	return nil
}

// GetCurrentMigrationState returns a mapping of partition keys to their current
// recorded timestamp.
func (*PostgresDialect) GetCurrentMigrationState(tx Transaction) (*MigrationState, error) {
	sqlQuery := `
		SELECT partition_key, timestamp FROM migration_metadata
		ORDER BY partition_key;
	`

	rows, err := tx.Query(sqlQuery)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := NewMigrationState()

	for rows.Next() {
		var partitionKey string
		var timestamp uint64

		err := rows.Scan(&partitionKey, &timestamp)
		if err != nil {
			return nil, err
		}

		result.Set(partitionKey, timestamp)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return result, nil
}

// SetMigrationStateForPartitionKey sets the timestamp associated with the
// provided partition key.
func (*PostgresDialect) SetMigrationStateForPartitionKey(tx Transaction, partitionKey string, timestamp uint64) error {
	sqlUpdate := `
		UPDATE migration_metadata SET timestamp=$2 WHERE partition_key=$1;
	`

	_, err := tx.Exec(sqlUpdate, partitionKey, timestamp)
	return err
}
