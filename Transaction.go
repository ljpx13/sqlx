package sqlx

import "database/sql"

// Transaction defines the methods of *sql.Tx that are expected to be used with
// this package.
type Transaction interface {
	Commit() error
	Rollback() error

	Exec(query string, args ...interface{}) (sql.Result, error)
	Query(query string, args ...interface{}) (*sql.Rows, error)
	QueryRow(query string, args ...interface{}) *sql.Row
}

var _ Transaction = &sql.Tx{}
