package sqlx

import (
	"context"
	"errors"
	"fmt"
	"sort"

	"gitlab.com/ljpx13/logging"
)

// MigrationPlan represents a set of steps required to migrate a database from
// its existing state to the newly provided state.
type MigrationPlan struct {
	db      Database
	dialect Dialect
	logger  logging.Logger

	orderedMigrations map[string][]Migration
	lookup            map[string]Migration
	desiredState      *MigrationState
	planHasRun        bool
}

// NewMigrationPlan creates a new MigrationPlan with the intent to migrate the
// database db to the desiredState using the provided migrations.
func NewMigrationPlan(db Database, dialect Dialect, logger logging.Logger, desiredState *MigrationState, migrations []Migration) (*MigrationPlan, error) {
	orderedMigrations := getOrderedMigrations(migrations)
	lookup := getMigrationLookup(migrations)
	newestMigrations := getNewestMigrations(orderedMigrations)

	for _, k := range desiredState.Keys() {
		newestMigrationForPartition, ok := newestMigrations[k]
		if !ok {
			return nil, fmt.Errorf("the provided desiredState describes a partition key '%v' that is not defined in the provided migrations", k)
		}

		timestamp, _ := desiredState.Get(k)
		if timestamp == 0 {
			timestamp = newestMigrationForPartition.Timestamp()
			desiredState.Set(k, timestamp)
		}

		_, ok = lookup[fmt.Sprintf("%v:%v", k, timestamp)]
		if !ok {
			return nil, fmt.Errorf("a migration with timestamp %v in partition '%v' does not exist, but is used in the desiredState", timestamp, k)
		}
	}

	return &MigrationPlan{
		db:      db,
		dialect: dialect,
		logger:  logger,

		orderedMigrations: orderedMigrations,
		lookup:            lookup,
		desiredState:      desiredState,
	}, nil
}

// Execute the migration plan.  Can only be executed once.
func (p *MigrationPlan) Execute(ctx context.Context) error {
	if p.planHasRun {
		return errors.New("this migration plan has already executed")
	}

	p.planHasRun = true

	tx, err := p.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}

	if err := p.dialect.EnsureMigrationMetadataTableExists(tx); err != nil {
		return err
	}

	for k := range p.orderedMigrations {
		if err := p.dialect.EnsureMigrationPartitionKeyExists(tx, k); err != nil {
			return err
		}
	}

	currentState, err := p.dialect.GetCurrentMigrationState(tx)
	if err != nil {
		return err
	}

	for _, k := range p.desiredState.Keys() {
		currentTimestamp, _ := currentState.Get(k)
		desiredTimestamp, _ := p.desiredState.Get(k)

		if err := p.executePartitionKey(k, currentTimestamp, tx); err != nil {
			p.logger.Printf("[SQL Migration] '%v' %v → %v: %v\n", k, currentTimestamp, desiredTimestamp, err)
			return err
		}

		if currentTimestamp != desiredTimestamp {
			p.logger.Printf("[SQL Migration] '%v' %v → %v: Success!\n", k, currentTimestamp, desiredTimestamp)
		} else {
			p.logger.Printf("[SQL Migration] '%v' %v = %v: No Changes\n", k, currentTimestamp, desiredTimestamp)
		}
	}

	if err := tx.Commit(); err != nil {
		p.logger.Printf("[SQL Migration] Changes failed to commit: %v\n", err)
		return err
	}

	p.logger.Printf("[SQL Migration] Changes successfully committed!\n")
	return nil
}

func (p *MigrationPlan) executePartitionKey(k string, currentTimestamp uint64, tx Transaction) error {
	desiredTimestamp, _ := p.desiredState.Get(k)
	if desiredTimestamp == currentTimestamp {
		return nil
	}

	orderedMigrationsInPartition := p.orderedMigrations[k]

	if desiredTimestamp < currentTimestamp {
		for i := len(orderedMigrationsInPartition) - 1; i >= 0; i-- {
			migration := orderedMigrationsInPartition[i]
			timestamp := migration.Timestamp()

			if timestamp > desiredTimestamp && timestamp <= currentTimestamp {
				if err := migration.Down(tx); err != nil {
					return err
				}

				if err := p.dialect.SetMigrationStateForPartitionKey(tx, k, timestamp); err != nil {
					return err
				}

				p.logger.Printf("[SQL Migration] '%v' ↓ %v (%v)\n", k, timestamp, migration.Name())
			}
		}
	} else {
		for i := 0; i < len(orderedMigrationsInPartition); i++ {
			migration := orderedMigrationsInPartition[i]
			timestamp := migration.Timestamp()

			if timestamp <= desiredTimestamp && timestamp > currentTimestamp {
				if err := migration.Up(tx); err != nil {
					return err
				}

				if err := p.dialect.SetMigrationStateForPartitionKey(tx, k, timestamp); err != nil {
					return err
				}

				p.logger.Printf("[SQL Migration] '%v' ↑ %v (%v)\n", k, timestamp, migration.Name())
			}
		}
	}

	return nil
}

func getMigrationLookup(migrations []Migration) map[string]Migration {
	result := map[string]Migration{}

	for _, migration := range migrations {
		key := fmt.Sprintf("%v:%v", migration.PartitionKey(), migration.Timestamp())
		result[key] = migration
	}

	return result
}

func getOrderedMigrations(migrations []Migration) map[string][]Migration {
	result := map[string][]Migration{}

	for _, m := range migrations {
		partitionKey := m.PartitionKey()
		list, _ := result[partitionKey]
		list = append(list, m)
		result[partitionKey] = list
	}

	for _, list := range result {
		sort.Slice(list, func(i int, j int) bool {
			return list[i].Timestamp() < list[j].Timestamp()
		})
	}

	return result
}

func getNewestMigrations(orderedMigrations map[string][]Migration) map[string]Migration {
	result := map[string]Migration{}

	for k, list := range orderedMigrations {
		result[k] = list[len(list)-1]
	}

	return result
}
