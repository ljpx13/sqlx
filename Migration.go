package sqlx

// Migration defines the methods that a migration must implement.  Migrations
// with distinct partition keys should not be able to interact or interfere with
// each other.
type Migration interface {
	Name() string
	PartitionKey() string
	Timestamp() uint64

	Up(tx Transaction) error
	Down(tx Transaction) error
}
