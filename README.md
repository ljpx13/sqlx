![](./icon.png)

# sqlx

Package `sqlx` provides a number of SQL-related abstractions and utilities,
including an easier way to perform database migrations.

## Local Testing

To test locally, ensure you have `docker`, `go` and `make` available on your
PATH, then run `make`.
