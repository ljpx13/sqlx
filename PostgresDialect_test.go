package sqlx

import (
	"context"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestPostgresDialect(t *testing.T) {
	// Arrange.
	db, err := getPostgresConnectionForTests()
	test.That(t, err, is.Nil())
	defer cleanupPostgresDatabase(db)

	tx, err := db.BeginTx(context.Background(), nil)
	test.That(t, err, is.Nil())

	dialect := NewPostgresDialect()

	// Act.
	err = dialect.EnsureMigrationMetadataTableExists(tx)
	test.That(t, err, is.Nil())

	err = dialect.EnsureMigrationPartitionKeyExists(tx, "pk1")
	test.That(t, err, is.Nil())

	err = dialect.EnsureMigrationPartitionKeyExists(tx, "pk2")
	test.That(t, err, is.Nil())

	err = dialect.SetMigrationStateForPartitionKey(tx, "pk2", 5)
	test.That(t, err, is.Nil())

	err = dialect.EnsureMigrationPartitionKeyExists(tx, "pk2")
	test.That(t, err, is.Nil())

	// Assert.
	migrationState, err := dialect.GetCurrentMigrationState(tx)
	test.That(t, err, is.Nil())
	test.That(t, migrationState.Count(), is.EqualTo(2))

	pk1, ok := migrationState.Get("pk1")
	test.That(t, ok, is.True())
	test.That(t, pk1, is.EqualTo(uint64(0)))

	pk2, ok := migrationState.Get("pk2")
	test.That(t, ok, is.True())
	test.That(t, pk2, is.EqualTo(uint64(5)))

	tx.Rollback()
}
