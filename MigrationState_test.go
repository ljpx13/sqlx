package sqlx

import (
	"encoding/json"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/has"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestMigrationStateBadPair(t *testing.T) {
	testCases := []string{
		"hello",
		"pk1:1:1",
		":",
		"pk1:pk2",
	}

	for _, testCase := range testCases {
		// Arrange and Act.
		_, err := NewMigrationStateFromString(testCase)

		// Assert.
		test.That(t, err, is.NotNil())
	}
}

func TestMigrationStateFromStringSuccess(t *testing.T) {
	// Arrange.
	str := "   pk1 :1,  pk2: 2  "

	// Act.
	m, err := NewMigrationStateFromString(str)

	// Assert.
	test.That(t, err, is.Nil())
	test.That(t, m.Count(), is.EqualTo(2))

	pk1, ok1 := m.Get("pk1")
	test.That(t, ok1, is.True())
	test.That(t, pk1, is.EqualTo(uint64(1)))

	pk2, ok2 := m.Get("pk2")
	test.That(t, ok2, is.True())
	test.That(t, pk2, is.EqualTo(uint64(2)))
}

func TestMigrationStateGetSetRemoveCountKeys(t *testing.T) {
	// Arrange.
	m := NewMigrationState()

	// Act.
	v1, ok1 := m.Get("pk")
	c1 := m.Count()
	m.Set("pk", 1234)
	v2, ok2 := m.Get("pk")
	c2 := m.Count()
	keys := m.Keys()
	m.Remove("pk")
	v3, ok3 := m.Get("pk")
	c3 := m.Count()

	// Assert.
	test.That(t, v1, is.EqualTo(uint64(0)))
	test.That(t, ok1, is.False())
	test.That(t, c1, is.EqualTo(0))
	test.That(t, v2, is.EqualTo(uint64(1234)))
	test.That(t, ok2, is.True())
	test.That(t, c2, is.EqualTo(1))
	test.That(t, v3, is.EqualTo(uint64(0)))
	test.That(t, ok3, is.False())
	test.That(t, c3, is.EqualTo(0))
	test.That(t, keys, has.Length(1))
	test.That(t, keys[0], is.EqualTo("pk"))
}

func TestMigrationStateMarshaling(t *testing.T) {
	// Arrange.
	m1 := NewMigrationState()
	m1.Set("pk1", 1)
	m1.Set("pk2", 2)

	// Act.
	raw, err := json.Marshal(m1)
	test.That(t, err, is.Nil())

	m2 := &MigrationState{}
	err = json.Unmarshal(raw, m2)
	test.That(t, err, is.Nil())

	// Assert.
	test.That(t, m2.Count(), is.EqualTo(2))

	pk1, ok1 := m2.Get("pk1")
	test.That(t, pk1, is.EqualTo(uint64(1)))
	test.That(t, ok1, is.True())

	pk2, ok2 := m2.Get("pk2")
	test.That(t, pk2, is.EqualTo(uint64(2)))
	test.That(t, ok2, is.True())
}

func TestMigrationStateUnmarshalOfInvalidStringRepresentation(t *testing.T) {
	// Arrange.
	raw := []byte(`"pk1:1:1,pk2:2"`)

	// Act.
	m := &MigrationState{}
	err := json.Unmarshal(raw, m)

	// Assert.
	test.That(t, err, is.NotNil())
}
