package sqlx

import "database/sql"

// MySQLDialect provides an interface into MySQL SQL servers.
type MySQLDialect struct{}

var _ Dialect = &MySQLDialect{}

// NewMySQLDialect creates a new MySQLDialect.
func NewMySQLDialect() *MySQLDialect {
	return &MySQLDialect{}
}

// EnsureMigrationMetadataTableExists will create a table for tracking migration
// metadata if one does not exist already.
func (*MySQLDialect) EnsureMigrationMetadataTableExists(tx Transaction) error {
	sqlCreate := `
		CREATE TABLE IF NOT EXISTS migration_metadata (
			partition_key VARCHAR(255) NOT NULL           PRIMARY KEY,
			timestamp     BIGINT       NOT NULL DEFAULT 0
		);
	`

	_, err := tx.Exec(sqlCreate)
	return err
}

// EnsureMigrationPartitionKeyExists will create an entry in the migration
// metadata table if one does not exist currently.
func (*MySQLDialect) EnsureMigrationPartitionKeyExists(tx Transaction, partitionKey string) error {
	sqlQuery := `
		SELECT partition_key FROM migration_metadata
		WHERE partition_key = ?;
	`

	sqlInsert := `
		INSERT INTO migration_metadata (partition_key)
		VALUES(?);
	`

	row := tx.QueryRow(sqlQuery, partitionKey)
	err := row.Scan(&partitionKey)
	if err == sql.ErrNoRows {
		_, err := tx.Exec(sqlInsert, partitionKey)
		return err
	}
	if err != nil {
		return err
	}

	return nil
}

// GetCurrentMigrationState returns a mapping of partition keys to their current
// recorded timestamp.
func (*MySQLDialect) GetCurrentMigrationState(tx Transaction) (*MigrationState, error) {
	sqlQuery := `
		SELECT partition_key, timestamp FROM migration_metadata
		ORDER BY partition_key;
	`

	rows, err := tx.Query(sqlQuery)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	result := NewMigrationState()

	for rows.Next() {
		var partitionKey string
		var timestamp uint64

		err := rows.Scan(&partitionKey, &timestamp)
		if err != nil {
			return nil, err
		}

		result.Set(partitionKey, timestamp)
	}

	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return result, nil
}

// SetMigrationStateForPartitionKey sets the timestamp associated with the
// provided partition key.
func (*MySQLDialect) SetMigrationStateForPartitionKey(tx Transaction, partitionKey string, timestamp uint64) error {
	sqlUpdate := `
		UPDATE migration_metadata SET timestamp=? WHERE partition_key=?;
	`

	_, err := tx.Exec(sqlUpdate, timestamp, partitionKey)
	return err
}
