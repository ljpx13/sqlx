package sqlx

import (
	"encoding/json"
	"fmt"
	"sort"
	"strconv"
	"strings"
)

// MigrationState represents a set of partition keys and their associated
// timestamps.
//
// The following is an example of a MigrationState in string form:
// main:202004071259,auth:202004021110
type MigrationState struct {
	m map[string]uint64
}

var _ fmt.Stringer = &MigrationState{}
var _ json.Marshaler = &MigrationState{}
var _ json.Unmarshaler = &MigrationState{}

// NewMigrationState creates a new, empty MigrationState with no partition keys
// in it.
func NewMigrationState() *MigrationState {
	return &MigrationState{
		m: map[string]uint64{},
	}
}

// NewMigrationStateFromString parses the provided string representation of a
// MigrationState to a real instance.
func NewMigrationStateFromString(stringRepresentation string) (*MigrationState, error) {
	stringRepresentation = strings.TrimSpace(stringRepresentation)
	commaSplit := strings.Split(stringRepresentation, ",")

	result := NewMigrationState()

	for _, pair := range commaSplit {
		colonSplit := strings.Split(pair, ":")
		if len(colonSplit) != 2 {
			return nil, fmt.Errorf("the pair '%v' was not a valid migration state pair", pair)
		}

		partitionKey := strings.TrimSpace(colonSplit[0])
		if partitionKey == "" {
			return nil, fmt.Errorf("partition keys cannot be empty")
		}

		timestampStr := strings.TrimSpace(colonSplit[1])
		timestamp, err := strconv.ParseUint(timestampStr, 10, 64)
		if err != nil {
			return nil, err
		}

		result.Set(partitionKey, timestamp)
	}

	return result, nil
}

// Get acts like a map lookup on the MigrationState, returning a value and a
// flag indicating if the value was found.
func (m *MigrationState) Get(partitionKey string) (uint64, bool) {
	v, ok := m.m[partitionKey]
	return v, ok
}

// Set sets the timestamp associated with the provided partitionKey.
func (m *MigrationState) Set(partitionKey string, timestamp uint64) {
	m.m[partitionKey] = timestamp
}

// Remove removes a partition key from the MigrationSet.
func (m *MigrationState) Remove(partitionKey string) {
	delete(m.m, partitionKey)
}

// Count returns the number of pairs in the MigrationState.
func (m *MigrationState) Count() int {
	return len(m.m)
}

// Keys returns all the partition keys in the MigrationState.
func (m *MigrationState) Keys() []string {
	results := make([]string, len(m.m))
	i := 0

	for k := range m.m {
		results[i] = k
		i++
	}

	return results
}

// String returns the string representation of the MigrationState.
func (m *MigrationState) String() string {
	keys := make([]string, len(m.m))
	i := 0

	for k := range m.m {
		keys[i] = k
		i++
	}

	sort.Strings(keys)

	result := ""

	for i, k := range keys {
		if i != 0 {
			result += ","
		}

		result += fmt.Sprintf("%v:%v", k, m.m[k])
	}

	return result
}

// MarshalJSON returns the JSON representation of a MigrationState, which is the
// same result as calling String.
func (m *MigrationState) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`"%v"`, m.String())), nil
}

// UnmarshalJSON accepts the JSON representation of a MigrationState and parses
// it back to a MigrationState instance using NewMigrationStateFromString.
func (m *MigrationState) UnmarshalJSON(raw []byte) error {
	stringRepresentation := strings.Trim(string(raw), `"`)

	nms, err := NewMigrationStateFromString(stringRepresentation)
	if err != nil {
		return err
	}

	*m = *nms
	return nil
}
