package sqlx

import (
	"context"
	"testing"

	"gitlab.com/ljpx13/logging"
	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/has"
	"gitlab.com/ljpx13/test/verbs/is"
)

func setupMigrationPlanTests() []Migration {
	return []Migration{
		&testMigration1{},
		&testMigration2{},
		&testMigration3{},
	}
}

func TestNewMigrationPlanUndefinedPartitionKey(t *testing.T) {
	// Arrange.
	migrationSet := setupMigrationPlanTests()
	desiredState, err := NewMigrationStateFromString("partition1:202004031147,partition2:202004031208,partition3:202004032209")

	// Act.
	migrationPlan, err := NewMigrationPlan(nil, nil, nil, desiredState, migrationSet)

	// Assert.
	test.That(t, migrationPlan, is.Nil())
	test.That(t, err, is.NotNil())
	test.That(t, err.Error(), is.EqualTo("the provided desiredState describes a partition key 'partition3' that is not defined in the provided migrations"))
}

func TestNewMigrationPlanUndefinedTimestamp(t *testing.T) {
	// Arrange.
	migrationSet := setupMigrationPlanTests()
	desiredState, err := NewMigrationStateFromString("partition1:202004031147,partition2:202004032209")

	// Act.
	migrationPlan, err := NewMigrationPlan(nil, nil, nil, desiredState, migrationSet)

	// Assert.
	test.That(t, migrationPlan, is.Nil())
	test.That(t, err, is.NotNil())
	test.That(t, err.Error(), is.EqualTo("a migration with timestamp 202004032209 in partition 'partition2' does not exist, but is used in the desiredState"))
}

func TestNewMigrationPlanSuccess(t *testing.T) {
	// Arrange.
	migrationSet := setupMigrationPlanTests()
	desiredState, err := NewMigrationStateFromString("partition1:202004031147,partition2:202004031208")

	// Act.
	migrationPlan, err := NewMigrationPlan(nil, nil, nil, desiredState, migrationSet)

	// Assert.
	test.That(t, migrationPlan, is.NotNil())
	test.That(t, err, is.Nil())
}

func TestMigrationPlanExecuteSuccessForward(t *testing.T) {
	// Arrange.
	migrationSet := setupMigrationPlanTests()
	dialect := NewPostgresDialect()
	logger := logging.NewTestLogger()

	db, err := getPostgresConnectionForTests()
	test.That(t, err, is.Nil())
	defer cleanupPostgresDatabase(db)

	// Act.
	desiredState, err := NewMigrationStateFromString("partition1:202004031153,partition2:202004031208")
	test.That(t, err, is.Nil())

	migrationPlan, err := NewMigrationPlan(db, dialect, logger, desiredState, migrationSet)
	test.That(t, err, is.Nil())

	err = migrationPlan.Execute(context.Background())

	// Assert.
	test.That(t, err, is.Nil())
	assertTestMigration1IsUp(t, db)
	assertTestMigration2IsUp(t, db)
	assertTestMigration3IsUp(t, db)
}

func TestMigrationPlanExecuteSuccessBackward(t *testing.T) {
	// Arrange.
	migrationSet := setupMigrationPlanTests()
	dialect := NewPostgresDialect()
	logger := logging.NewTestLogger()

	db, err := getPostgresConnectionForTests()
	test.That(t, err, is.Nil())
	defer cleanupPostgresDatabase(db)

	desiredState1, err := NewMigrationStateFromString("partition1:0,partition2:202004031208")
	test.That(t, err, is.Nil())

	migrationPlan1, err := NewMigrationPlan(db, dialect, logger, desiredState1, migrationSet)
	test.That(t, err, is.Nil())

	err = migrationPlan1.Execute(context.Background())
	test.That(t, err, is.Nil())

	// Act.
	desiredState2, err := NewMigrationStateFromString("partition1:202004031147")
	test.That(t, err, is.Nil())

	migrationPlan2, err := NewMigrationPlan(db, dialect, logger, desiredState2, migrationSet)
	test.That(t, err, is.Nil())

	err = migrationPlan2.Execute(context.Background())

	// Assert.
	test.That(t, err, is.Nil())
	assertTestMigration1IsUp(t, db)
	assertTestMigration2IsDown(t, db)
	assertTestMigration3IsUp(t, db)
}

func TestGetOrderedMigrations(t *testing.T) {
	// Arrange.
	migrationSet := setupMigrationPlanTests()

	// Act.
	migrations := getOrderedMigrations(migrationSet)

	// Assert.
	test.That(t, migrations, is.NotNil())
	test.That(t, migrations, has.Length(2))
	test.That(t, migrations["partition1"], has.Length(2))
	test.That(t, migrations["partition1"][0], is.EqualTo(migrationSet[0]))
	test.That(t, migrations["partition1"][1], is.EqualTo(migrationSet[1]))
	test.That(t, migrations["partition2"], has.Length(1))
	test.That(t, migrations["partition2"][0], is.EqualTo(migrationSet[2]))
}

func TestGetMigrationLookup(t *testing.T) {
	// Arrange.
	migrationSet := setupMigrationPlanTests()

	// Act.
	lookup := getMigrationLookup(migrationSet)

	// Assert.
	test.That(t, lookup, has.Length(3))
	test.That(t, lookup["partition1:202004031147"], is.EqualTo(migrationSet[0]))
	test.That(t, lookup["partition1:202004031153"], is.EqualTo(migrationSet[1]))
	test.That(t, lookup["partition2:202004031208"], is.EqualTo(migrationSet[2]))
}

func TestGetNewestMigrations(t *testing.T) {
	// Arrange.
	migrationSet := setupMigrationPlanTests()
	orderedMigrations := getOrderedMigrations(migrationSet)

	// Act.
	newestMigrations := getNewestMigrations(orderedMigrations)

	// Assert.
	test.That(t, newestMigrations, has.Length(2))
	test.That(t, newestMigrations["partition1"], is.EqualTo(migrationSet[1]))
	test.That(t, newestMigrations["partition2"], is.EqualTo(migrationSet[2]))
}

func assertTestMigration1IsUp(t *testing.T, db Database) {
	t.Helper()

	sqlInsert := `
		INSERT INTO users (user_id, first_name, last_name)
		VALUES ('1234', 'John', 'Smith');
	`

	_, err := db.ExecContext(context.Background(), sqlInsert)
	test.That(t, err, is.Nil())

	sqlDelete := `
		DELETE FROM users WHERE user_id='1234';
	`

	_, err = db.ExecContext(context.Background(), sqlDelete)
	test.That(t, err, is.Nil())
}

func assertTestMigration2IsUp(t *testing.T, db Database) {
	t.Helper()

	sqlInsert := `
		INSERT INTO users (user_id, first_name, middle_name, last_name)
		VALUES ('1235', 'John', 'Jarrod', 'Smith');
	`

	_, err := db.ExecContext(context.Background(), sqlInsert)
	test.That(t, err, is.Nil())

	sqlDelete := `
		DELETE FROM users WHERE user_id='1235';
	`

	_, err = db.ExecContext(context.Background(), sqlDelete)
	test.That(t, err, is.Nil())
}

func assertTestMigration2IsDown(t *testing.T, db Database) {
	t.Helper()

	sqlInsert := `
		INSERT INTO users (user_id, first_name, middle_name, last_name)
		VALUES ('1235', 'John', 'Jarrod', 'Smith');
	`

	_, err := db.ExecContext(context.Background(), sqlInsert)
	test.That(t, err, is.NotNil())

	if err == nil {
		sqlDelete := `
		DELETE FROM users WHERE user_id='1235';
	`

		_, err = db.ExecContext(context.Background(), sqlDelete)
		test.That(t, err, is.Nil())
	}
}

func assertTestMigration3IsUp(t *testing.T, db Database) {
	t.Helper()

	sqlInsert := `
		INSERT INTO orders (order_id, amount)
		VALUES ('1236', 1200.01);
	`

	_, err := db.ExecContext(context.Background(), sqlInsert)
	test.That(t, err, is.Nil())

	sqlDelete := `
		DELETE FROM orders WHERE order_id='1236';
	`

	_, err = db.ExecContext(context.Background(), sqlDelete)
	test.That(t, err, is.Nil())
}

func assertTestMigration3IsDown(t *testing.T, db Database) {
	t.Helper()

	sqlInsert := `
		INSERT INTO orders (order_id, amount)
		VALUES ('1236', 1200.01);
	`

	_, err := db.ExecContext(context.Background(), sqlInsert)
	test.That(t, err, is.NotNil())

	if err == nil {
		sqlDelete := `
		DELETE FROM orders WHERE order_id='1236';
	`

		_, err = db.ExecContext(context.Background(), sqlDelete)
		test.That(t, err, is.Nil())
	}
}

// -----------------------------------------------------------------------------

type testMigration1 struct{}

var _ Migration = &testMigration1{}

func (*testMigration1) Name() string {
	return "testMigration1"
}

func (*testMigration1) PartitionKey() string {
	return "partition1"
}

func (*testMigration1) Timestamp() uint64 {
	return 2020_04_03_11_47
}

func (*testMigration1) Up(tx Transaction) error {
	sqlCreate := `
		CREATE TABLE users (
			user_id    VARCHAR(16)  NOT NULL PRIMARY KEY,
			first_name VARCHAR(100) NOT NULL,
			last_name  VARCHAR(100) NOT NULL
		);
	`

	_, err := tx.Exec(sqlCreate)
	return err
}

func (*testMigration1) Down(tx Transaction) error {
	sqlDrop := `
		DROP TABLE users;
	`

	_, err := tx.Exec(sqlDrop)
	return err
}

type testMigration2 struct{}

var _ Migration = &testMigration2{}

func (*testMigration2) Name() string {
	return "testMigration2"
}

func (*testMigration2) PartitionKey() string {
	return "partition1"
}

func (*testMigration2) Timestamp() uint64 {
	return 2020_04_03_11_53
}

func (*testMigration2) Up(tx Transaction) error {
	sqlAlter := `
		ALTER TABLE users
		ADD COLUMN middle_name VARCHAR(100);
	`

	_, err := tx.Exec(sqlAlter)
	return err
}

func (*testMigration2) Down(tx Transaction) error {
	sqlDrop := `
		ALTER TABLE users
		DROP COLUMN middle_name;
	`

	_, err := tx.Exec(sqlDrop)
	return err
}

type testMigration3 struct{}

var _ Migration = &testMigration3{}

func (*testMigration3) Name() string {
	return "testMigration3"
}

func (*testMigration3) PartitionKey() string {
	return "partition2"
}

func (*testMigration3) Timestamp() uint64 {
	return 2020_04_03_12_08
}

func (*testMigration3) Up(tx Transaction) error {
	sqlCreate := `
		CREATE TABLE orders (
			order_id   VARCHAR(16)  NOT NULL PRIMARY KEY,
			amount     NUMERIC(12, 4)
		);
	`

	_, err := tx.Exec(sqlCreate)
	return err
}

func (*testMigration3) Down(tx Transaction) error {
	sqlDrop := `
		DROP TABLE orders;
	`

	_, err := tx.Exec(sqlDrop)
	return err
}
