package sqlx

import (
	"context"
	"database/sql"
	"fmt"
	"os"
	"time"

	_ "github.com/lib/pq" // Postgres
	"gitlab.com/ljpx13/policy"
)

var connectionRetryPolicy = policy.NewBackOff(7, 250)
var existingDatabaseConnection Database

func getPostgresConnectionForTests() (Database, error) {
	if existingDatabaseConnection != nil {
		return existingDatabaseConnection, nil
	}

	hostname := "localhost"
	hostnameFromEnv, ok := os.LookupEnv("SQLX_POSTGRES_HOSTNAME")
	if ok {
		hostname = hostnameFromEnv
	}

	time.Sleep(time.Second * 4)

	var db Database
	err := connectionRetryPolicy.Attempt(context.Background(), func() error {
		var err error

		db, err = sql.Open("postgres", fmt.Sprintf("host=%v user=subroot dbname=sqlx_test password=sqlx1234 sslmode=disable", hostname))
		if err != nil {
			return err
		}

		return db.Ping()
	})

	if err != nil {
		return nil, err
	}

	existingDatabaseConnection = db
	return existingDatabaseConnection, nil
}

func cleanupPostgresDatabase(db Database) {
	db.ExecContext(context.Background(), `
		DROP TABLE users;
	`)

	db.ExecContext(context.Background(), `
		DROP TABLE orders;
	`)

	db.ExecContext(context.Background(), `
		DROP TABLE migration_metadata;
	`)
}
