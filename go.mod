module gitlab.com/ljpx13/sqlx

go 1.14

require (
	github.com/lib/pq v1.3.0
	gitlab.com/ljpx13/policy v0.1.1
	gitlab.com/ljpx13/test v0.1.6
	gitlab.com/ljpx13/logging v0.1.2
)
