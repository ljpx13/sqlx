.PHONY: test

test:
	docker run --name sqlx-postgres-instance -d -p 5432:5432 -e POSTGRES_USER=subroot -e POSTGRES_PASSWORD=sqlx1234 -e POSTGRES_DB=sqlx_test postgres:12.2-alpine
	-/usr/local/go/bin/go test --race --cover ./...
	docker kill sqlx-postgres-instance
	docker rm sqlx-postgres-instance