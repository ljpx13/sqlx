package sqlx

import (
	"context"
	"database/sql"
)

// Database defines the methods of *sql.DB that are expected to be used with
// this package.
type Database interface {
	BeginTx(ctx context.Context, options *sql.TxOptions) (*sql.Tx, error)
	Ping() error

	ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error)
	QueryContext(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error)
	QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row
}

var _ Database = &sql.DB{}
