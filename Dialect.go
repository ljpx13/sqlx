package sqlx

// Dialect defines the methods that any SQL dialect must implement.  All methods
// interact with a SQL transaction to perform an operation on a database.
type Dialect interface {
	EnsureMigrationMetadataTableExists(tx Transaction) error
	EnsureMigrationPartitionKeyExists(tx Transaction, partitionKey string) error

	GetCurrentMigrationState(tx Transaction) (*MigrationState, error)
	SetMigrationStateForPartitionKey(tx Transaction, partitionKey string, timestamp uint64) error
}
